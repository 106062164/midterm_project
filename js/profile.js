function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
  }

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logoutBtn = document.getElementById('logout-btn');

            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    alert("Logout Success!");
                    window.location.href = "index.html";
                  }).catch(function(error) {
                    alert("Logout Error!");
                  });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('prof_info').innerHTML = "";
        }
    });

    edit_btn = document.getElementById('edit_btn');
    photo_btn = document.getElementById('photo_btn');
    var name = document.getElementById('name');
    var birthday = document.getElementById('birth');
    var address = document.getElementById('address');
    var gender = document.getElementById('gender');

    // The html code for post
    var total_post = [];

    var infoRef = firebase.database().ref('information');
    var exist = 0;

    infoRef.once('value')
    .then(function (snapshot) {
        var list = document.getElementById('prof_info');
        snapshot.forEach(function(data){
            if(data.val().email == user_email) {
                exist = 1;
                var key = data.key;
                loadData(key);
            }
        });
        list.innerHTML = total_post.join("");
        if(exist == 0) {
            createData();
        }
    })
    .catch(e => console.log(e.message));
};

function createData(key) {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logoutBtn = document.getElementById('logout-btn');

            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    alert("Logout Success!");
                    window.location.href = "index.html";
                  }).catch(function(error) {
                    alert("Logout Error!");
                  });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('prof_info').innerHTML = "";
        }
    });

    edit_btn = document.getElementById('edit_btn');
    photo_btn = document.getElementById('photo_btn');
    var name = document.getElementById('name');
    var birthday = document.getElementById('birth');
    var address = document.getElementById('address');
    var gender = document.getElementById('gender');

    edit_btn.addEventListener('click', function () {
        var database = firebase.database().ref('information');
        if(name.value != "" && birthday.value != "" && address.value != "" && gender.value != "") {
            database.push({
                email : user_email,
                name : name.value,
                birthday : birthday.value,
                address : address.value,
                gender : gender.value
            });
            name.value = "";
            birthday.value = "";
            address.value = "";
            gender.value = "";
            location.reload();
        }
    });

    // The html code for post
    var total_post = [];

    var infoRef = firebase.database().ref('information');

    infoRef.on('value')
    .then(function (snapshot) {
        var list = document.getElementById('prof_info');
        snapshot.forEach(function(data){
            if(data.val().email == user_email) {
                var key = data.key;
                loadData(key);
            }
        });
        list.innerHTML = total_post.join("");
    })
    .catch(e => console.log(e.message));
};

function loadData(key) {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logoutBtn = document.getElementById('logout-btn');

            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    alert("Logout Success!");
                    window.location.href = "index.html";
                  }).catch(function(error) {
                    alert("Logout Error!");
                  });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('prof_info').innerHTML = "";
        }
    });

    edit_btn = document.getElementById('edit_btn');
    photo_btn = document.getElementById('photo_btn');
    var name = document.getElementById('name');
    var birthday = document.getElementById('birth');
    var address = document.getElementById('address');
    var gender = document.getElementById('gender');

    edit_btn.addEventListener('click', function () {
        var database = firebase.database().ref('information').child(key);
        if(name.value != "" && birthday.value != "" && address.value != "" && gender.value != "") {
            database.set({
                email : user_email,
                name : name.value,
                birthday : birthday.value,
                address : address.value,
                gender : gender.value
            });
            name.value = "";
            birthday.value = "";
            address.value = "";
            gender.value = "";
            location.reload();
        }
    });

    // The html code for post
    var total_post = [];

    var infoRef = firebase.database().ref('information');

    infoRef.once('value')
    .then(function (snapshot) {
        var list = document.getElementById('prof_info');
        snapshot.forEach(function(data){
            if(data.key == key) {
                total_post.push("<div class='my-3 p-3 bg-white rounded box-shadow' id='content1'><h4>Profile Information</h4>");
                total_post.push("<p> Email : " + data.val().email + "</p>");
                total_post.push("<p> Name : " + data.val().name + "</p>");
                total_post.push("<p> Birthday : " + data.val().birthday + "</p>");
                total_post.push("<p> Address : " + data.val().address + "</p>");
                total_post.push("<p> Gender : " + data.val().gender + "</p></div>");
                total_post.push("<div class='my-3 p-3 bg-white rounded box-shadow' id='content2'><img src='img/profile.png' alt='' class='mr-2 rounded' style='height:213px; width:213px;'><br><br></div>");
            }
        });
        list.innerHTML = total_post.join("");
    })
    .catch(e => console.log(e.message));
};

window.onload = function () {
    init();
    startTime();
};