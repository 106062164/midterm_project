var total_post = [];

function notifyMe(message) {
    if (!("Notification" in window)) {
      alert("This browser does not support desktop notification");
    }
    else if (Notification.permission === "granted") {
      var notification = new Notification(message);
    }
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        if (permission === "granted") {
          var notification = new Notification(message);
        }
      });
    }
}

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
  }

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logoutBtn = document.getElementById('logout-btn');

            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    alert("Logout Success!");
                    window.location.href = "index.html";
                  }).catch(function(error) {
                    alert("Logout Error!");
                  });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_ttl = document.getElementById('title');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_ttl.value != "" && post_txt.value != "") {
            var database = firebase.database().ref('com_list');
            database.push({
                email : user_email,
                title : post_ttl.value,
                text : post_txt.value
            });
            post_ttl.value = "";
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/profile.png' alt='' class='mr-2 rounded' style='height:48px; width:48px;'><p><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>";

    var postsRef = firebase.database().ref('com_list');

    postsRef.once('value')
    .then(function (snapshot) {
        var list = document.getElementById('post_list');
        snapshot.forEach(function(data){
            total_post.push(str_before_username + data.val().email + "</strong>" + data.val().title + "<br><button class='btn btn-primary' id='" + data.val().title + "' onclick='openPost(this.id)'>Reply</button>" + str_after_content);
        });
        list.innerHTML = total_post.join("");
    })
    .catch(e => console.log(e.message));

    postsRef.on('child_added', function(data){
        var list = document.getElementById('post_list');
        list.innerHTML += str_before_username + data.val().email + "</strong>" + data.val().title + "<br><button class='btn btn-primary' id='" + data.val().title + "' onclick='openPost(this.id)'>Reply</button>" + str_after_content;
        
    });
};

function openPost(clicked_id) {
    var elem = document.getElementById("post_title");
    elem.remove();
    var elem = document.getElementById("title");
    elem.remove();
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logoutBtn = document.getElementById('logout-btn');

            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    alert("Logout Success!");
                    window.location.href = "index.html";
                  }).catch(function(error) {
                    alert("Logout Error!");
                  });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/profile.png' alt='' class='mr-2 rounded' style='height:48px; width:48px;'><p><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>";

    var textRef = firebase.database().ref('com_list');

    textRef.once('value')
    .then(function (snapshot) {
        total_post = [];
        var list = document.getElementById('post_list');
        snapshot.forEach(function(data){
            if(clicked_id == data.val().title) {
                var key = data.key;
                total_post.push(str_before_username + data.val().email + "</strong>" + data.val().text + str_after_content);
                loadPost(key);
            }
        });
        list.innerHTML = total_post.join("");
    })
    .catch(e => console.log(e.message));
};

function loadPost(key) {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logoutBtn = document.getElementById('logout-btn');

            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    alert("Logout Success!");
                    window.location.href = "index.html";
                  }).catch(function(error) {
                    alert("Logout Error!");
                  });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/profile.png' alt='' class='mr-2 rounded' style='height:48px; width:48px;'><p><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>";

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var database = firebase.database().ref('com_list').child(key).child("reply");
            database.push({
                email : user_email,
                text : post_txt.value
            });
            post_txt.value = "";
        }
    });

    var postsRef = firebase.database().ref('com_list').child(key).child("reply");
    postsRef.once('value')
    .then(function (snapshot) {
        var list = document.getElementById('post_list');
        snapshot.forEach(function(data){
            total_post.push(str_before_username + data.val().email + "</strong>" + data.val().text + str_after_content);
        });

        list.innerHTML = total_post.join("");
    })
    .catch(e => console.log(e.message));

    postsRef.on('child_added', function(data){
        var list = document.getElementById('post_list');
        list.innerHTML += str_before_username + data.val().email + "</strong>" + data.val().text + str_after_content;
        
    });
}

window.onload = function () {
    if(scr == 0) {
        init();
        startTime();
        notifyMe("Hello! Welcome to The Forum!");
    } else {
        openPost();
    }
};