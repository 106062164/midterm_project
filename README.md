# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : Forum
* Key functions
    1. Add new topic in the forum (post list page)
    2. Add any comment in any topic (post page, leave comment under any post)
    3. Create/update profile information (user page)
    
* Other functions
    1. Clock
    2. Audio Player

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://ss-midterm-project-223b6.firebaseapp.com/

# Components Description : 
1. Membership Mechanism
   We can login/register by using email and password. Each email can only have one account. Note that the password cannot be too short (it will be considered too weak).
2. Firebase Page
   I have used Firebase deploy to host my page. The URL can be seen on the top.
3. Database
   If you want to post any topic or leave any comment, you need to login first. In general, there are 2 kinds of data in the database. The first one is com_list, which contains topics and comments. The second one is information, which contains profile information of users.
4. RWD
   My website is responsive web design, the web pages render well on a variety of devices and window or screen sizes. 
5. Topic Key Function
   To access these functions, you need to login first, otherwise, it will not respond or provide any data.
   - Post List Page
     In the post list page (homepage), you can see the posts that have been created before. It only displays the post maker's email and the post title. To see the content and reply a comment, you can click on the reply button which is located under the post title. On the bottom, there is a form to create new post. Your post will not be submitted if post title or post content is empty.
   - Post Page & Leave Comment
     In the post page, you can see the topic content/problem on the top and the responses following. It displays the post maker's/responder's email and the content/response. On the bottom, there is a form to reply. Same with post list page, your response will not be submitted if it is empty.
   - User Page
     In the user page, you may or may not be able to see your current profile information. If you have not submitted your profile information before, then you will not see it, but if you have submitted your profile information before, then you will see it. You may create/update your profile information by filling the 'Edit Profile' form. Your name length cannot exceed 20 characters and your address length cannot exceed 50 characters. Again, your changes will be submitted only when all fields are not empty.
6. Third-Party Sign Up/In
   You can login/register with Google account.
7. Chrome Notification
   The chrome notification will appear when the homepage has been loaded. It also will appear when you successfully register a new account.
8. CSS Animation
   I make a ball animation. The ball is moving infinite times from left to right and vice versa, with various color and scale. The ball is located on the navigation bar.

# Other Functions Description(1~10%) : 
1. Clock
   I put the clock in case someone is too busy surfing on the forum, so it may reminds him/her to take a break and focus on real life. The clock is on the navigation bar.
2. Audio Player
   Surfing on the forum may become more enjoyable by listening to the music, so I add an audio player. The audio player is on the navigation bar.

## Security Report (Optional)
In this website, anyone who has logged in may post topic or leave any comment at any post, so the database can be written or read, as long as auth != null.
The thing which should be kept private is profile information. In the profile.js :

/*var infoRef = firebase.database().ref('information');
infoRef.on('value')
.then(function (snapshot) {
    var list = document.getElementById('prof_info');
    snapshot.forEach(function(data){
        if(data.val().email == user_email) {
            var key = data.key;
            loadData(key);
        }
    });
    list.innerHTML = total_post.join("");
})
.catch(e => console.log(e.message));*/

To access the profile information, I use the data key. Since each email may only has one account, it means our key can be obtained when data.val().email == user_email. This also means that we will not know the other's key. After that, we use that key to access our own information.
